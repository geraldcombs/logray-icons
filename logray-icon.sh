#!/bin/bash

set -e 

src_dir="$( dirname "$( stat -f %R "$0" )" )/../wireshark"
icon_dir="$src_dir/resources/icons"

cd "$src_dir/resources/icons"

pwd

gm convert -size 48x48 lricon256.png -resize 48x48 lricon48.png
gm convert -size 48x48 lriconinst256.png -resize 48x48 lriconinst48.png

for _i in lricon*.png ; do
    gm convert $_i -units PixelsPerInch -density 96 $_i
done

../../tools/compress-pngs.py lricon*.png

cd "$src_dir/packaging/macosx"

rm -rf Logray.{icns,iconset}

mkdir Logray.iconset

cd Logray.iconset

cp $icon_dir/lricon1024.png icon512x512@2x.png
cp $icon_dir/lricon512.png icon512x512.png
cp $icon_dir/lricon512.png icon256x256@2x.png
cp $icon_dir/lricon256.png icon256x256.png
cp $icon_dir/lricon256.png icon128x128@2x.png
cp $icon_dir/lricon128.png icon128x128.png
cp $icon_dir/lricon64.png icon32x32@2x.png
cp $icon_dir/lricon32.png icon32x32.png
cp $icon_dir/lricon32.png icon16x16@2x.png
cp $icon_dir/lricon16.png icon16x16.png

cd ..

iconutil -c icns Logray.iconset

rm -rf Logray.iconset

cd $icon_dir

# Create .ico by hand from 256x256, 48x48, 32x32, and 16x16.

# gm identify -format "%f %w x %h %x%U x %y%U\n" lricon[1-9]*.png | sort -V

file lricon*.png | sort -V
